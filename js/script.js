var dicionario = [
        {"cerveja":"Weissbier","tempMin":"-1° C","tempMax":"3° C"},
        {"cerveja":"Pilsens","tempMin":"-2° C","tempMax":"4° C"},
        {"cerveja":"Weizenbier","tempMin":"-4° C","tempMax":"6° C"},
        {"cerveja":"Red Ale","tempMin":"-5° C","tempMax":"5° C"},
        {"cerveja":"India Pale Ale","tempMin":"-6° C","tempMax":"7° C"},
        {"cerveja":"IPA","tempMin":"-7° C","tempMax":"10° C"},
        {"cerveja":"Dunkel","tempMin":"-8° C","tempMax":"2° C"},
        {"cerveja":"Imperial Stouts","tempMin":"-10° C","tempMax":"13° C"},
        {"cerveja":"Brown Ale","tempMin":"0° C","tempMax":"14° C"}
]

var listaUl = document.querySelector("#busca ul");

for(var i = 0; i< dicionario.length; i++){
    var li = document.createElement("li");
    var h3 = document.createElement("h3");
    var span = document.createElement("span");

    h3.innerText = dicionario[i].cerveja;
    span.innerText = "temperatura mínima " + dicionario[i].tempMin;

    li.appendChild(h3);

    li.appendChild(span);

    listaUl.appendChild(li);
}

document.getElementById("dicionario").style.display="none";

var pesquisaInput = document.querySelector("#pesquisa");

pesquisaInput.onkeyup = function(){

    var lis = document.querySelectorAll("#busca li");
        for (var i = 0; i < lis.length; i++){
            var termo = lis[i].querySelector("h3");
            lis[i].style.display = "block";
            if (termo.innerText.toLowerCase().search(this.value.toLowerCase()) > -1 && this.value!=""){
            	document.getElementById("dicionario").style.display="block";
            } 
            else {
                lis[i].style.display = "none";
                }
    }
}